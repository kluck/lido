#!/usr/bin/env python3

# sudo pip install python-osc

from pythonosc import dispatcher
from pythonosc import osc_server
import subprocess

note_mapping = { "67": "55", "60": "200" }

def note_on(addr, value):
    print("address: {}, value: {}".format(addr, value))
    note = addr.split("/")[-1]
    if (note in note_mapping):
        print("matched note: {}".format(note_mapping[note]))
        subprocess.run(["playerctl", "position", note_mapping[note]])

if __name__ == "__main__":
  dispatcher = dispatcher.Dispatcher()
  dispatcher.map("/life/*", note_on)
  # dispatcher.map("/*", print)

  server = osc_server.ThreadingOSCUDPServer(("localhost", 8000), dispatcher)
  print("Serving on {}".format(server.server_address))
  server.serve_forever()
